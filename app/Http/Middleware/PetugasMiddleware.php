<?php

namespace App\Http\Middleware;

use App\Databencana;
use Closure;
use Illuminate\Http\Response;

class PetugasMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->type != 'petugas')
        {
//            return new Response(view('unauthorized')->with('role', 'PETUGAS'));
        }
        return $next($request);
    }
}
