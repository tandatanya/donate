<?php

namespace App\Http\Controllers;
use App\Datadonasi;
use App\User;
use App\Datapetugas;
use App\Penjemputan;
use Illuminate\Http\Response;
use App\Databencana;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function distribusi(){
        $userAddresses = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("penjemputan.status", "3")
            ->get();
//            dd($userAddresses);
        return view('distribusi',compact(['userAddresses']));
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin(Request $request){
        return view('middleware')->withMassage("Admin");
    }

    public function petugas(Request $request){
        return view('middleware')->withMessage("Petugas");
    }

    public function member(Request $request){
        return view('middleware')->withMessage("Member");
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $distribusi = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("penjemputan.status", "3")
            ->get();

        if ($request->user() && $request->user()->type == 'admin')
        {
            return view('home');
        }
        elseif ($request->user() && $request->user()->type == 'petugas')
        {
            if($request->user() && $request->user()->status == '1'){
            return view('home');
        }
        else{
                return view('welcome')->with('danger','Akun Anda belum terverifikasi');
        }
        }
        else{
            $dataslider = Databencana::all()->take(3)->get();
            $databencana = Databencana::get();
            return view('welcome',compact(['databencana','dataslider']));
        }
//        return view('home');
    }

    public function rekap($id){
        $userAddresses = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("penjemputan.user_id", $id)
            ->get();

//        dd($userAddresses);
        $penjemputan = User::find($id)->orders();
//        $penjemputan = Penjemputan::find($id)->orders;
//        $penjemputan = Penjemputan::with('datadonasi')
//            ->whereColumn('kode_donasi','datadonasis.id')
//            ->whereColumn('datadonasi.kode_bencana','databencana.id')
//            ->where('kode_donatur',$id)
//            ->get();
//        $datadonasi = Datadonasi::find($penjemputan->datadonasi_id);
        return view('rekapdonasi',compact(['userAddresses']));
    }

//    public function someAdminStuff(Request $request)
//    {
//        $request->user()->authorizeRoles('petugas');
//    }

}
