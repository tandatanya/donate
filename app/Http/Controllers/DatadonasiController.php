<?php

namespace App\Http\Controllers;

use App\Penjemputan;
use Illuminate\Http\Request;
use App\Databencana;
use App\Datadonasi;
use App\User;
use Illuminate\Http\Response;

class DatadonasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

//    public function petugas(Request $request){
//        return view('middleware')->withMessage("Petugas");
//    }


    public function jemputan($id){
        $penjemputan = Penjemputan::find($id);
        return view('datadonasi.editpenjemputan',compact(['penjemputan']));
    }

    public function index()
    {
        $datadonasi = User::select("users.*","datadonasis.*")
        ->join("datadonasis", "users.id", "=", "datadonasis.user_id")
//        ->where("penjemputan.user_id", $id)
        ->get();
//        dd($datadonasi);
//        $datadonasi = Datadonasi::all()->toArray();
        return view('datadonasi.index', compact('datadonasi'));
    }
//    public function berhasil(){
//        return view('datadonasi.berhasil')->with('success', 'Data ditambahkan');
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $databencana = Databencana::find($id);
        return view('datadonasi.create', compact('databencana'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $databencana = Databencana::get();
        $this->validate($request, [
            'user_id'   =>  'required',
            'databencana_id'    =>  'required',
            'jenis_barang'     =>  'required',
			'donasi'           =>  'required',
			'jumlah'           =>  'required',
			'keterangan'       =>  'required',
			'lokasi'           =>  'required'
        ]);
       $input['user_id']        =   $request->user_id;
       $input['databencana_id'] =   $request->databencana_id;
       $input['jenis_barang']   =   $request->jenis_barang;
       $input['donasi']         =   $request->donasi;
       $input['jumlah']         =   $request->jumlah;
       $input['keterangan']     =   $request->keterangan;
       $input['lokasi']         =   $request->lokasi;
       Datadonasi::create($input);
        return new Response(view('welcome',compact('databencana')));
//        $datadonasi = new Datadonasi([
//            'kode_donasi'      =>  $request->get('kode_donasi'),
//            'jenis_barang'     =>  $request->get('jenis_barang'),
//			'donasi'           =>  $request->get('donasi'),
//			'jumlah'           =>  $request->get('jumlah'),
//			'keterangan'       =>  $request->get('keterangan'),
//			'lokasi'           =>  $request->get('lokasi')
//        ]);
//        $datadonasi->save();
//        return redirect()->route('')->with('success','Donasi berhasil ditambahkan');
        //return view('welcome')->with('success','Donasi berhasil ditambahkan')
//        return redirect()->route('datadonasi.berhasil   ')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datadonasi = Datadonasi::find($id);
//        $datadonasi = User::select("users.*","datadonasis.*")->join("datadonasis", "users.id", "=", "datadonasis.user_id")->where("datadonasis.id", $id)->get();

//        dd($datadonasi);
        return view('datadonasi.edit', compact(['datadonasi','id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
//            'user_id'      =>  'required',
//            'databencana_id'=>'required',
            'jenis_barang' => 'required',
            'donasi' => 'required',
            'jumlah' => 'required',
            'keterangan' => 'required',
            'lokasi' => 'required'
        ]);
        $datadonasi = Datadonasi::find($id);
        $datadonasi->id = $request->get('kode_donasi');
//        $datadonasi->user_id = $request->get('user_id');
//        $datadonasi->databencana_id = $request->get('databencana_id');
        $datadonasi->jenis_barang = $request->get('jenis_barang');
        $datadonasi->donasi = $request->get('donasi');
        $datadonasi->jumlah = $request->get('jumlah');
        $datadonasi->keterangan = $request->get('keterangan');
        $datadonasi->lokasi = $request->get('lokasi');
        $datadonasi->save();


        $input['datadonasi_id'] =   $request->kode_donasi;
        $input['user_id']        =   $datadonasi->user_id;
        $input['status']        = $request->status;
        $input['tanggal']       = $request->tanggal;
        Penjemputan::create($input);

        $datadonasi = User::select("users.*","datadonasis.*")
            ->join("datadonasis", "users.id", "=", "datadonasis.user_id")
//        ->where("penjemputan.user_id", $id)
            ->get();
        return new Response(view('datadonasi.index',compact('datadonasi')));
    }

    public function update1(Request $request, $id)
    {

        $penjemputan = Penjemputan::find($id);
        $input['datadonasi_id'] =   $penjemputan->datadonasi_id;
        $input['user_id']        =   $penjemputan->user_id;
        $input['status']        = $request->status;
        $input['tanggal']       = $request->tanggal;
        Penjemputan::create($input);

        $datadonasi = User::select("users.*","datadonasis.*")
            ->join("datadonasis", "users.id", "=", "datadonasis.user_id")
//        ->where("penjemputan.user_id", $id)
            ->get();
        return new Response(view('datadonasi.index',compact('datadonasi')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datadonasi = Datadonasi::find($id);
        $datadonasi->delete();
        return redirect()->route('datadonasi.index')->with('success', 'Data Deleted');
    }


    public function distribusi(){
//        $datadonasi = Penjemputan::get()
//            ->where("penjemputan.datadonasis_id","datadonasis.id")
//            ->get();
        $datadonasi = Datadonasi::select("datadonasis.*","penjemputan.*")
            ->join("penjemputan", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->get();
        //            ->where("penjemputan.datadonasi_id", "datadonasis.id")
//            dd($userAddresses);
        return view('datadonasi.distribusi',compact(['datadonasi']));
    }
}
