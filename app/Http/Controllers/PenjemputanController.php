<?php

namespace App\Http\Controllers;
use App\Penjemputan;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class PenjemputanController extends Controller
{
    //
    public function update(Request $request, $id)
    {

        $penjemputan = Penjemputan::find($id);
        $input['datadonasi_id'] =   $penjemputan->datadonasi_id;
        $input['user_id']        =   $penjemputan->user_id;
        $input['status']        = $request->status;
        $input['tanggal']       = $request->tanggal;
        Penjemputan::create($input);

        $datadonasi = User::select("users.*","datadonasis.*")
            ->join("datadonasis", "users.id", "=", "datadonasis.user_id")
//        ->where("penjemputan.user_id", $id)
            ->get();
        return new Response(view('datadonasi.distribusi',compact('datadonasi')));
    }
}
