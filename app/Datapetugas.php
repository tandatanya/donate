<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datapetugas extends Model
{
    protected $fillable = ['id', 'nama_petugas', 'jadwal_bekerja', 'alamat_bertugas', 'kendaraan', 'job'];
}
