<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datadonatur extends Model
{
    protected $fillable = ['id', 'nama_donatur', 'jumlah_donasi', 'status', 'nomor_handphone', 'alamat'];
}
