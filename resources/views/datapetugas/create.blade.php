@extends('master')

@section('content')
<div class="row">
 <div class="col-md-12">
  <br />
  <h3 aling="center">Tambah Data</h3>
  <br />
  @if(count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
   @foreach($errors->all() as $error)
    <li>{{$error}}</li>
   @endforeach
   </ul>
  </div>
  @endif
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif

  <form method="post" action="{{url('datapetugas')}}">
   {{csrf_field()}}
   <div class="form-group">
    <input type="text" name="id" class="form-control" placeholder="Masukkan ID" />
   </div>
   <div class="form-group">
    <input type="text" name="nama_petugas" class="form-control" placeholder="Masukkan Nama Petugas" />
   </div>
   <div class="form-group">
    <input type="text" name="jadwal_bekerja" class="form-control" placeholder="Masukkan Jadwal Bekerja" />
   </div>
   <div class="form-group">
    <input type="text" name="alamat_bertugas" class="form-control" placeholder="Masukkan Alamat Bertugas" />
   </div>
   <div class="form-group">
    <input type="text" name="kendaraan" class="form-control" placeholder="Masukkan Kendaraan Yang Dipakai" />
   </div>
   <div class="form-group">
    <input type="text" name="job" class="form-control" placeholder="Masukkan Job" />
   </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" />
   </div>
  </form>
 </div>
</div>
@endsection