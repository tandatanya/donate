@extends('layouts.app1')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Bencana
                <small>Tambah Data Bencana</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Input Data Bencana</a></li>
                <li class="active">Creates</li>
            </ol>
        </section>


        <section class="content-header">
            <div class="row">
                <div class="col-md-12">
                    <br />
                    <h3 align="center">Data Bencana</h3>
                    <br />
                    @if($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{$message}}</p>
                        </div>
                    @endif
                    <div align="right">
                        <a href="{{route('databencana.create')}}" class="btn btn-primary">Tambah</a>
                        <br />
                        <br />
                    </div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Kode Bencana</th>
                            <th>Judul Bencana</th>
                            <th>Deskripsi Singkat Bencana</th>
                            <th>Deskripsi Lengkap Bencana</th>
                            <th>
                                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                    Gambar
                                </div>
                            </th>
                            <th>Lokasi Bencana</th>
                            {{--<th>Edit</th>--}}
                            <th>Hapus</th>
                        </tr>

                        @foreach($databencana as $row)
                            <tr>
                                <td>{{$row['kode_bencana']}}</td>
                                <td>{{$row['title']}}</td>
                                <td>{{$row['highligt']}}</td>
                                <td>{{$row['deskripsi_bencana']}}</td>
                                <td>
                                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                                        <img src="/images/{{ $row->image }}" style="width: 100%;"/>
                                    </div>
                                </td>
                                <td>{{$row['lokasi_bencana']}}</td>
                                {{--<td><a href="{{action('DatabencanaController@edit', $row['id'])}}" class="btn btn-warning">Edit</a></td>--}}
                                <td>
                                    <form method="post" class="delete_form" action="{{action('DatabencanaController@destroy', $row['id'])}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
            <script>
                $(document).ready(function(){
                    $('.delete_form').on('submit', function(){
                        if(confirm("Are you sure you want to delete it?"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    });
                });
            </script>
        </section>
    </div>
@endsection