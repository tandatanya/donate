@extends('master')

@section('content')

<div class="row">
 <div class="col-md-12">
  <br />
  <h3>Edit Data Donatur</h3>
  <br />
  @if(count($errors) > 0)

  <div class="alert alert-danger">
         <ul>
         @foreach($errors->all() as $error)
          <li>{{$error}}</li>
         @endforeach
         </ul>
  @endif
  <form method="post" action="{{action('DatadonaturController@update', $no)}}">
   {{csrf_field()}}
   <input type="hidden" name="_method" value="PATCH" />
   <div class="form-group">
    <input type="text" name="id" class="form-control" value="{{$datadonatur->id}}" placeholder="Masukkan ID" />
   </div>
   <div class="form-group">
    <input type="text" name="nama_donatur" class="form-control" value="{{$datadonatur->nama_donatur}}" placeholder="Masukkan Nama Donatur" />
   </div>
   <div class="form-group">
    <input type="text" name="jumlah_donasi" class="form-control" value="{{$datadonatur->jumlah_donasi}}" placeholder="Masukkan Jumlah Donasi" />
   </div>
   <div class="form-group">
    <input type="text" name="status" class="form-control" value="{{$datadonatur->status}}" placeholder="Masukkan Status" />
   </div>
   <div class="form-group">
    <input type="text" name="nomor_handphone" class="form-control" value="{{$datadonatur->nomor_handphone}}" placeholder="Masukkan Nomor Handphone" />
   </div>
   <div class="form-group">
    <input type="text" name="alamat" class="form-control" value="{{$datadonatur->alamat}}" placeholder="Masukkan Alamat" />
   </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" value="Edit" />
   </div>
  </form>
 </div>
</div>

@endsection