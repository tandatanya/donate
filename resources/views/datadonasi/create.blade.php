@extends('master')
@section('title')
 <title>Formulir Donasi</title>
 @endsection

@section('content')
 <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

 <div class="about-area area-padding">
 <div class="container">
 <div class="row">
  @guest
   <br/>
  <br/>
  <div  class="text-center">
   <h3> Anda Belum Login Silahkan Login terlebih dahulu</h3>
   <a href="{{url('/login')}}" class="btn btn-warning">LOGIN</a>
  </div>
   @else
   <br />
   <div class="text-center">
   <h1>Tambah Bantuan Donasi</h1>
  </div>
  <hr>
   <br />
   @if(count($errors) > 0)
    <div class="alert alert-danger">
     <ul>
      @foreach($errors->all() as $error)
       <li>{{$error}}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if(\Session::has('success'))
    <div class="alert alert-success">
     <p>{{ \Session::get('success') }}</p>
    </div>
   @endif


    <form method="post" action="{{url('datadonasi')}}">
     {{csrf_field()}}
     <div class="table table-bordered table-hover" id="tab_logic">

      <div class="container">
       <div class="row clearfix">
        <div class="col-md-12 column">
         <table class="table table-bordered table-hover" id="tab_logic">
          <thead>
          <tr >
           <th class="text-center">
            #
           </th>
           <th class="text-center">
            Kode Donatur
           </th>
           <th class="text-center">
            Kode Bencana
           </th>
           <th class="text-center">
            Jenis Barang
           </th>
           <th class="text-center">
            Donasi
           </th>
           <th class="text-center">
            Jumlah
           </th>
           <th class="text-center">
            Keterangan
           </th>
          </tr>
          </thead>
          <tbody>
          <tr id='addr0'>
           <td>
            1
           </td>
           <td>
            <input type="text" name="user_id" class="form-control" value="{{Auth::user()->id}}" placeholder="{{Auth::user()->id}}" />
           </td>
           <td>
            <input type="text" name="databencana_id" class="form-control" value="{{$databencana->id}}" placeholder="{{$databencana->id}}" />
           </td>
           <td>
            <select class="form-control" name="jenis_barang">
             <option value="Sandang">Sandang</option>
             <option value="Pangan">Pangan</option>
             <option value="Papan">Papan</option>
            </select>
           </td>
           <td>
            <input type="text" name='donasi' placeholder='Donasi' class="form-control"/>
           </td>
           <td>
            <input type="text" name='jumlah' placeholder='jumlah' class="form-control"/>
           </td>
           <td>
            <input type="text" name='keterangan' placeholder='keterangan' class="form-control"/>
           </td>
          </tr>
          <tr id='addr1'></tr>
          </tbody>
         </table>
        </div>
       </div>
       {{--<a href="#" id="add_row" class="btn btn-primary pull-right">Tambahkan</a>--}}
       {{--<button id="add_row" class="btn btn-primary pull-right" >Tambahkan</button>--}}
      </div>
      <div class="form-group">
       <b><p>Lokasi Bencana</p></b>
       <input type="text" name="lokasi" value="{{$databencana->lokasi_bencana}}" placeholder="{{$databencana->lokasi_bencana}}" >
      </div>
      <div class="form-group">
       <input type="submit" class="btn btn-primary" />
      </div>
     </div>
    </form>
    @endguest


  </div>
 </div>





 </div>

<script>
    $(document).ready(function(){
        var i=1;
        $("#add_row").click(function(){
            $('#addr'+(i-1)).find('input').attr('disabled',true);
            $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input type='text' name='kode_donasi"+i+"'  placeholder='{{$databencana->id}}' class='form-control input-md' disabled/></td> <td><select class='form-control' name='jenis_barang"+i+"'><option>Sandang</option><option>Pangan</option><option>Papan</option> </td><td><input type='text' name='donasi"+i+"' placeholder='donasi' class='form-control input-md'/></td><td><input type='text' name='jumlah"+i+"' placeholder='jumlah' class='form-control input-md'/></td><td><input type='text' name='keterangan"+i+"' placeholder='keterangan' class='form-control input-md'/></td>");

            $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
            i++;
        });
    });

</script>

@endsection