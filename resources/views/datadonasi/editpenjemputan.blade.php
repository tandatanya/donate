<?php $number=1; ?>
@extends('layouts.app1')
@section('title')
 <title>Donate|Data Donasi</title>
@endsection


@section('content')

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <h1>
    Donasi
    <small>Penjemputan Data Donasi</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dahsboard</a></li>
    <li><a href="#"><i class="fa fa-database"></i>Donasi </a></li>
    <li class="active">Data Donasi</li>
   </ol>
  </section>

  <section class="content-header">
   <div class="row">
    <div class="col-md-12">
     <br />
     <h3>Edit Data</h3>
     <br />
     @if(count($errors) > 0)
      <div class="alert alert-danger">
       <ul>
        @foreach($errors->all() as $error)
         <li>{{$error}}</li>
        @endforeach
       </ul>
       @endif
      </div>
      <form method="post" action="{{action('PenjemputanController@update', $penjemputan->id)}}" enctype="multipart/form-data">
       {{csrf_field()}}
       <input type="hidden" name="_method" value="PATCH" />

        <div class="form-group row">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Status Donasi')}}</label>
         <div class="col-md-6">
          <select class="form-control" name="status" id="status">
           @if($penjemputan->status==0)
            <option value="0" selected>Belum dijemput</option>
            <option value="1">Sedang dijemput</option>
            <option value="2">Sudah dijemput</option>
            <option value="3">Terdistribusi</option>
           @endif
            @if($penjemputan->status==1)
             <option value="0">Belum dijemput</option>
             <option value="1" selected>Sedang dijemput</option>
             <option value="2">Sudah dijemput</option>
             <option value="3">Terdistribusi</option>
            @endif
            @if($penjemputan->status==2)
             <option value="0">Belum dijemput</option>
             <option value="1">Sedang dijemput</option>
             <option value="2" selected>Sudah dijemput</option>
             <option value="3">Terdistribusi</option>
            @endif
            @if($penjemputan->status==3)
             <option value="0">Belum dijemput</option>
             <option value="1">Sedang dijemput</option>
             <option value="2">Sudah dijemput</option>
             <option value="3" selected>Terdistribusi</option>
            @endif
          </select>
         </div>
        </div>

        <div class="form-group row">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Tanggal Penjemputan')}}</label>
         <div class="col-md-6">
         <input type="date" name="tanggal" class="form-control" value="{{$penjemputan->tanggal}}"  placeholder="Masukkan Tanggal" />
         </div>
        </div>

        <div class="form-group">
         <input type="submit" class="btn btn-primary" value="Edit" />
        </div>
       </form>
      </div>
    </div>
  </section>
   </div>
@endsection
